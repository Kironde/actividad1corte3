import { MonsterTypeI } from "../interfaces/MonsterTypeI";
import { PokemonI } from "../interfaces/PokemonInterfaces";
const db = require('../db/Pokemons.json');

module PokemonsService { 
    export function getAll(): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        return pokemons
    }
    export function get(id: number): PokemonI {
        const pokemons: Array<PokemonI> = db;
        const pokemon: Array<PokemonI> = pokemons.filter(e => e.id === id);
        if (pokemon.length < 1) {
            throw "No se encontró el digimon"
        }
        return pokemon[0];
    }
    export function getByName(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = pokemons.filter(function(el) {
            return el.name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").indexOf(name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "")) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        let matches: Array<PokemonI> = [];
        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(pokemon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        }
        return matches;
    }

    export function StrongAgainst(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = getByName(name);
        let Debiles: Array<PokemonI> = [];

        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === matches[0].type[0].strongAgainst[0]);
            if (found.length>0) {
                Debiles.push(pokemon);
            }
        })

        if (Debiles.length < 1) {
            throw matches[0].type[0].strongAgainst[0];
        }

        return Debiles;
    }

    export function WeakAgainst(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = getByName(name);
        let Debiles: Array<PokemonI> = [];

        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === matches[0].type[0].weakAgainst[0]);
            if (found.length>0) {
                Debiles.push(pokemon);
            }
        })

        if (Debiles.length < 1) {
            throw matches[0].type[0].strongAgainst[0];
        }

        return Debiles;
    }

    export function Create(id:string ,name: string, type :string, strongAgainst:string, weakAgainst:string, Img:string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        db.push({
            id: id,
            name: name,
            type: [{
                type,
                strongAgainst,
                weakAgainst,
            }],
            img: Img,
        })

        return db;
    }


}

export default PokemonsService;