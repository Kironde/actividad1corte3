import { Request, Response } from "express";
import PokemonsService from "../services/PokemonService";

export function getAll(_: any, res: Response) {
    const pokemons = PokemonsService.getAll();
    res.status(200).json(pokemons);
}

export function get(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del pokemon."}
        const pokemons = PokemonsService.get(id);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.getByName(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type && req.params.type || undefined;
        if(!type){ throw "Se requiere el Tipo del pokemon."}
        const pokemons = PokemonsService.getByType(type);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function StrongAgainst(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.StrongAgainst(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function WeakAgainst(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.WeakAgainst(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function Create(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        const type = req.params.type && req.params.type || undefined;
        const img = req.params.img && req.params.img || undefined;
        const strongAgainst = req.params.strongAgainst && req.params.strongAgainst || undefined;
        const weakAgainst = req.params.weakAgainst && req.params.weakAgainst || undefined;
        const id = req.params.id && req.params.id || undefined;
        if(!id || !name || !type || !img || !strongAgainst || !weakAgainst){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.Create(id,name, type, strongAgainst, weakAgainst, img);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}